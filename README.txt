JSSE Proxy
==========

OSB has two SSL implementations: Certicom and JSSE.

Certicom is the default. It is though a bit outdated, and cannot support newer certificates and ciphers.

JSSE is newer, but dropped support for SSLv2 (including SSLv2Hello message).

When a domain has to support both new and old backend services, neither implementation supports all of them. There is no way to enable JSSE only for new services, too.

A long-term goal is to switch all backend services to TLS 1.2+, and switch OSB to JSSE. Until then, new services are to be routed into a local JSSE proxy, 
a web application that establishes JSSE connection to the target domain over TLS. 

This application will be outdated when the domain is switched to JSSE.

Usage:

	In Biz service that needs to use TLS, point it to the same domain with the /jsseproxy prefix, e.g.:
	
	Target URL: https://host:port/a/b/c
	Point Biz to: http://esbdomain/jsseproxy/host:port/a/b/c
	
	If the service needs to use a proxy:
	
	Target URL: https://host:port/a/b/c
	Point Biz to: http://esbdomain/jsseproxy/(proxy=proxy.internal:8080)/host:port/a/b/c 

Supported properties:
	
	proxy=host:port
	timeout=N (read timeout in milliseconds)
    connecttimeout=N (connect timeout in milliseconds; applied to both proxy and remote host)
	debug=true|yes (logs handshake and data exchange steps into system out)

	
	