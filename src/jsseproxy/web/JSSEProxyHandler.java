package jsseproxy.web;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.security.SecureRandom;
import java.util.Enumeration;
import java.util.Properties;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JSSEProxyHandler {

	private static SecureRandom secureRandom;
//	private static KeyStore serverKeyStore;
//	private static KeyStore clientKeyStore;
//	private static TrustManagerFactory tmf;
//	private static KeyManagerFactory kmf;
	private static SSLContext sslContext;
	private static SSLSocketFactory sf;
	
	private StringBuilder log = new StringBuilder();
	private Properties props = new Properties();
	
	boolean debug = false;
	
	public void handle(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		Socket ps = null;
		SSLSocket socket = null;

		InputStream sis = null;
		OutputStream sos = null;
		
		ServletInputStream rr = req.getInputStream();
		OutputStream rw = resp.getOutputStream();
		
		parseUrl(req);
		String url = props.getProperty("URL");
		
		try {
			init();

			String backendUrl = "https:/"+url;
			URL backend = new URL(backendUrl);
			
			appendToLog("Backend "+backendUrl);
			
			String host = backend.getHost();
			int port = backend.getPort();
			if( port == -1 ) port = 443;
			String uri = backend.getPath();
			
			appendToLog("Host "+host);
			appendToLog("Port "+port);
			appendToLog("URI "+uri);
			
			// if via proxy, open a tunnel first
			boolean useProxy = props.getProperty("proxy") != null;
			appendToLog("use proxy:" +useProxy+" <- "+props.getProperty("proxy"));
			if( useProxy ) {
				URL proxyUrl = new URL("http://"+props.getProperty("proxy"));
		    	String proxyHost = proxyUrl.getHost();
		    	int proxyPort = proxyUrl.getPort();
		    	if( proxyPort == -1 ) proxyPort = 80;
		    	appendToLog("Using proxy "+proxyHost+":"+proxyPort);
		    	
		    	// ps = new Socket(proxyHost, proxyPort);

				String cto = props.getProperty("connecttimeout");			
				appendToLog("Connect timeout: "+cto);			
		    	
		    	ps = new Socket();   
		    	ps.connect(new InetSocketAddress(proxyHost,proxyPort),cto!=null?Integer.parseInt(cto):5000); 		    	
		    	
		    	OutputStream os = ps.getOutputStream();
		    	
		    	String connect = "CONNECT "+host+":"+port+" HTTP/1.0\r\n\r\n";
		    	appendToLog(connect);
		    	os.write(connect.getBytes());
		    	os.flush();
		    	
		    	appendToLog("Reading proxy response");
		    	
		    	InputStream is = ps.getInputStream();
		    	LineNumberReader lnr = new LineNumberReader(new InputStreamReader(is));
		    	
		    	boolean proxyOK = true;
		    	while(true) {
		    		String line = lnr.readLine();
		    		appendToLog(line);
		    		if( line == null || line.trim().length() == 0 ) break;
		    		
		    		if( lnr.getLineNumber() == 1 ) {
		    			if( line.startsWith("HTTP/1.1 200") || line.startsWith("HTTP/1.0 200") ) {
		    				// OK, got our tunnel
		    			} else {
		    				proxyOK = false;
		    			}
		    		}
		    	}
		    	
		    	if( !proxyOK ) {
    				throw new IOException("Failed to tunnel through proxy");
		    	}
			}
	
			// create a data socket
			if( ps != null ) {
				// using proxy socket
				socket = (SSLSocket)sf.createSocket( ps, host, port, true );			
			} else {
				String cto = props.getProperty("connecttimeout");			
				appendToLog("Connect timeout: "+cto);			

				socket = (SSLSocket)sf.createSocket();
				InetSocketAddress ha = new InetSocketAddress(host, port);
				socket.connect(ha,cto!=null?Integer.parseInt(cto):5000);
			}
			
			String to = props.getProperty("timeout");			
			appendToLog("timeout: "+to);			
			socket.setSoTimeout(to!=null?Integer.parseInt(to):30000);			
	
			// readers/writers
			sos = socket.getOutputStream();
			sis = socket.getInputStream();

			ByteArrayOutputStream content = new ByteArrayOutputStream();
			byte buf[] = new byte[1024]; 
			while(true) {
				int read = rr.read(buf);
				if( read < 0 ) break;
				content.write(buf,0,read);
			}

			appendToLog("Sending request");			
			
			// POST URI HTTP/1.1
			String post = "POST "+uri+" HTTP/1.1\r\n";
			sos.write(post.getBytes());
			appendToLog(post);
			
			boolean sentContentLength = false;
			
			// copy all headers except Host
			Enumeration<String> headerNames = req.getHeaderNames();
			while( headerNames.hasMoreElements() ) {
				String headerName = headerNames.nextElement();
				
				if( "Host".equalsIgnoreCase(headerName) ) {
					appendToLog("Skipped: "+headerName);
					continue;
				}
	
				if( "Connection".equalsIgnoreCase(headerName) ) {
					appendToLog("Skipped: "+headerName);
					continue;
				}

				if( "Transfer-Encoding".equalsIgnoreCase(headerName) ) {
					appendToLog("Skipped: "+headerName);
					continue;
				}
				
				if( "Content-Length".equalsIgnoreCase(headerName) ) {
					sentContentLength = true;
				}
				
				Enumeration<String> headerValues = req.getHeaders(headerName);
				while( headerValues.hasMoreElements() ) {
					String headerValue = headerValues.nextElement();
					String hh = headerName+": "+headerValue+"\r\n";
					sos.write(hh.getBytes());
					appendToLog(hh);
				}
			}
			
			// provide Host header 
			String hh = "Host: "+host+"\r\n";
			sos.write(hh.getBytes());
			appendToLog("Added: "+hh);

			// request to close the connection to detect the end of the payload
			hh = "Connection: close\r\n";
			sos.write(hh.getBytes());
			appendToLog("Added: "+hh);
	
			// backends may hang if no content-length is provided, and OSB doesn't send it 
			if( !sentContentLength ) {
				hh = "Content-Length: "+content.size()+"\r\n";
				sos.write(hh.getBytes());
				appendToLog("Added: "+hh);
			}
			
			// empty line before content
			sos.write("\r\n".getBytes());
			appendToLog("\r\n");
			
			// write content
			sos.write(content.toByteArray());
			appendToLog(new String(content.toByteArray()));
			
			sos.flush();
			
			appendToLog("Reading response");			
			
			// reading the response 
			readResponseHeaders(resp, sis);
			
			appendToLog("Response headers are read");
			
			// copy the whole response to client
			while(true) {
				int read = sis.read(buf);
				if( read < 0 ) break;
				
				rw.write(buf,0,read);
				appendToLog(new String(buf,0,read));
			}
			
			appendToLog("Response is read. Sending it to the client.");
			rw.flush();
			
			appendToLog("Exchange completed.");
		} catch( Exception ex ) {
			ex.printStackTrace(System.err);
			try {
				appendToLog(ex);
				resp.setStatus(500);
				if( rw != null ) rw.write(log.toString().getBytes());
				if( rw != null ) rw.flush();
			} catch (IOException e) {
			}
		} finally {
			if( ps != null ) try{ ps.close(); } catch( Exception ex ){}
			if( socket != null ) try{ socket.close(); } catch( Exception ex ){}
				
			if( rr != null ) try{ rr.close(); } catch( Exception ex ){}
			if( rw != null ) try{ rw.close(); } catch( Exception ex ){}

			if( sis != null ) try{ sis.close(); } catch( Exception ex ){}
			if( sos != null ) try{ sos.close(); } catch( Exception ex ){}
		}
	}

	private void parseUrl(HttpServletRequest req) throws IOException {
		String uri = req.getRequestURI(); 	// /jsseproxy/(proxy=foo.com:1234)/domain.com:5678/path/to/service
		String ctx = req.getContextPath(); 	// /jsseproxy
		
		appendToLog("Context path: "+ctx);
		
		if( ctx.length() > 0 ) {
			uri = uri.substring(ctx.length(),uri.length());
			appendToLog("URL after context: "+uri);
		}
		
		if( uri.startsWith("/(") ) {
			// parse properties
			int n = uri.indexOf(")/");
			if( n <= 0 ) throw new IOException("malformed URL: cannot find the closing )/: "+uri);
			
			String p = uri.substring(2,n);
			p = p.replaceAll(",","\n");
			
			props.load(new StringReader(p));
			appendToLog("properties: "+props);
			
			uri = uri.substring(n+1); // starts with /
		}

		appendToLog("URL: "+uri);
		props.put("URL",uri);
		
		if( "true".equalsIgnoreCase(props.getProperty("debug")) || "yes".equalsIgnoreCase(props.getProperty("debug")) ) {
			debug = true;
		}
	}

	private void appendToLog(Exception ex) {
		StringWriter sw = new StringWriter();
		ex.printStackTrace(new PrintWriter(sw));
		log.append("Exception:\n\n").append(sw.toString());
		if( debug ) {
			ex.printStackTrace(System.out);
		}
	}

	private void appendToLog(String string) {
		if( string == null ) string = "<null>";
		string = string.trim();
		
		if( log.length() == 0 ) {
			log.append("Communication log\n");
			log.append("========================\n");
			log.append("\n");
		}
		log.append(string).append('\n');
		if( debug ) {
			System.out.println(string);
			System.out.flush();
		}
	}

	private void readResponseHeaders(HttpServletResponse resp, InputStream sr) throws IOException {
		LineNumberReader lnr = new LineNumberReader(new InputStreamReader(sr));
		
		// reading the status
		String status = lnr.readLine();
		appendToLog(status);
		
		String[] se = status.split("\\s+",3);
		resp.setStatus(Integer.parseInt(se[1]),se.length>2?se[2]:"-");
		
		// reading the headers and copying them to the client
		while(true) {
			String header = lnr.readLine();
			if( header.trim().length() == 0 ) break;
			appendToLog(header);
			
			String hdr[] = header.split(":",2);
			resp.addHeader(hdr[0].trim(),hdr.length>1?hdr[1].trim():"");
		}
	}

	private static synchronized void init() throws Exception {
		if( sf != null ) return; // another thread has already initialized the handler

		secureRandom = new SecureRandom();
		secureRandom.nextInt();
		
// TODO: extract from the config.xml using serializedSystem.dat
//		serverKeyStore = KeyStore.getInstance( "JKS" );
//		serverKeyStore.load( new FileInputStream( "trust.jks" ), "trust_password".toCharArray() );
//
//		clientKeyStore = KeyStore.getInstance( "JKS" );
//		clientKeyStore.load( new FileInputStream( "identity.jks" ), "identity_password".toCharArray() );
//
//		tmf = TrustManagerFactory.getInstance( "SunX509" );
//		tmf.init( serverKeyStore );
//
//		kmf = KeyManagerFactory.getInstance( "SunX509" );
//		kmf.init( clientKeyStore, "cert_password".toCharArray() ); 

		sslContext = SSLContext.getInstance("TLS");
//		sslContext.init( kmf.getKeyManagers(), tmf.getTrustManagers(), secureRandom );
		sslContext.init( null, new TrustManager[]{new GullibleTrustManager()}, secureRandom );
		
		sf = sslContext.getSocketFactory();
	}	
}
